from django.db import models

# Create your models here.
from django.urls import reverse
from django.utils.text import slugify
from datetime import datetime as dt


from django.contrib.auth.models import User

from member.models import Member


class Post(models.Model):
    owner = models.ForeignKey(Member, on_delete=models.CASCADE)
    title =  models.CharField(max_length=200, help_text='eneter post title...')
    body =  models.TextField( help_text='eneter teh content of the post...')
    pub_date = models.DateTimeField(default=dt.now())
    # change_date = models.DateTimeField(default=dt.now())
    slug = models.SlugField( null=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super().save(*args, **kwargs)



    def __str__(self):
        return f"{self.pub_date.year}==> {self.title[:15]}...{self.owner.user.username}"

    def get_absolute_url(self):
        return reverse('weblog_post_detail',
                       kwargs={'slug':self.slug})


# side code
spec_c = ["'", ":", ","]
def clean_slug(spec_c):
    for p in Post.objects.all():
        for c in spec_c:
            if c in p.slug:
                p.slug = p.slug.replace(c, "_")
                p.save()


def my_slugify():
    for p in Post.objects.all():
        p.slug =  p.title.replace(' ', '-').lower()[:25]
        p.save()

##  task to do  User authentication/authorization
# 1. restructure the project_420
# a. make base.html, static css/images ...  at project level
#
# 2. create an app that will handle users and their profiles
# means, remove author model
# for member app, create authentication processes:
#     a.create    member application(one - to - one with User)
#     b.logging
#     c. logout
#     d. register
#


