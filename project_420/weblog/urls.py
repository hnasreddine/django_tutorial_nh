from django.urls import path
from . import views
urlpatterns = [

    # path ('home', views.home, name='weblog_home' ),
    # path ('contact', views.contact, name='weblog_contact' ),
    path ('about', views.about, name='weblog_about' ),
    # path ('list-all/', views.list_all, name='weblog_list_all' ),
#--- testing with CBV
    path ('', views.MyHomeView.as_view(), name='weblog_home' ),
    path('home', views.MyHomeView.as_view(),name='weblog_home' ),
    path ('contact', views.MyContactTPLView.as_view(), name='weblog_contact' ),

    # path ('list-all-view', views.MyListPostsView.as_view(), name='weblog_list_all' ),
    # path('list-all-temp', views.MyListPostsTempView.as_view(), name='weblog_list_all'),
    path ('list-all', views.MyListPosts.as_view(), name='weblog_list_all'),

    # path ('list-authors', views.MyListAuthors.as_view(), name='weblog_list_authors'),
    # path(list)
    path('post/<slug:slug>/', views.MyPostDetail.as_view(), name='weblog_post_detail'),


#---  CUD using functions
path('new-post/', views.CreatePostClassView.as_view(), name='weblog_post_create'),
path('edit-post/<int:pk>', views.UpdatePostClassView.as_view(), name='weblog_post_edit'),
path('delete-post/<int:pk>', views.RemovePostClassView.as_view(), name='weblog_post_delete'),

#--------------------------- REST PAI routes/paths
    path('api', views.api_map, name='api_map'),
    path('api/post-list', views.api_get_all_posts, name='api_post_list'),
    path('api/post-new', views.api_create_post, name='api_post_new'),
    path('api/<int:pk>/post-detail', views.api_post_detail, name='api_post_detail'),
    path('api/post-edit/<int:pk>', views.api_edit_post, name='api_post_edit'),
    path('api/<int:pk>/post-delete', views.api_delete_post, name='api_post_delete'),






]