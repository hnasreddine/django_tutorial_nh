from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from .models import Post

def home (req):
    data = {'page_title':"Home page",
            'greet': "Welcome to Home Page"}
    return render(req, 'weblog/home.html',context=data)


def contact (req):
    data = {'page_title': "Contact page",
            'greet': "Contact Info Page",
            "prof_list": [{'name': 'nasr', 'office': '3h14', 'contact': 'by MIO'},
                          {'name': 'dirk', 'office': '3F.26 ', 'contact': 'ask him'},
                          {'name': 'andrew', 'office': '3h26', 'contact': 'by MIO'},
                          ]
            }
    return render(req, 'weblog/contact.html', context=data )

def about (req):
    data = {'page_title': "About page",
            'greet': "About Info Page"}

    return render(req, 'weblog/about.html',
                    context=data )

# def get_post_2017_lss():
#     p_17 = Post.objects.all().filter(pub_date__year__lte=2017)
#     return p_17


from .models import Post

def get_posts_2017():
    return Post.objects.filter(pub_date__year=2017)

def list_all (req):
    data = {'page_title': "List page",
            'greet': "list Posts data",
            'list_posts' : get_posts_2017(),
            # 'stats_list':get_posts_stats(),
            'lst_stat': get_auth_posts_totals(5)
            }
    template_name= 'weblog/post_list.html'
    return render(req, template_name, context=data )


from django.db.models import Count
from member.models import Member
def get_auth_posts_totals(top_n):
    return Member.objects.annotate(count_posts= Count('post')).order_by('-count_posts')[:top_n]

# def get_posts_stats():
#     from django.db.models import Count
#     return Author.objects.annotate(nber_posts = Count('post'))\
#         .order_by('-nber_posts')[:6]

#1 update the app
# a. update href links in template to use variable name from url
# b. add right side bar for statistics
# c. use annotate: for each  author, get the name and the number of posts

#-----------------------  CLASS BASED VIEWS -----------
#  2. add the generic View class, and also TemplateView
#  a. to replace the functions views.
# b. use View class and then TemplateView  to display a list of objects
# c. use View class and then TemplateView  to display a single object
# 3.----- Now use specialized Generic CBV  -- ListView and DetailView
# to optimize the code and DRY

from django.views.generic import View, TemplateView
class MyHomeView(View):
     tempate_name = 'weblog/home.html'
     def get(self, req,  *args, **kwargs):
         data = {'page_title': "Home page (CBV)",
                 'greet': "Welcome to (CBV) Home Page"}
         return render (req, self.tempate_name, context = data)

data = {'page_title': "Contact page",
            'greet': "Contact Info Page",
            "prof_list": [{'name': 'nasr', 'office': '3h14', 'contact': 'by MIO'},
                          {'name': 'dirk', 'office': '3F.26 ', 'contact': 'ask him'},
                          {'name': 'andrew', 'office': '3h26', 'contact': 'by MIO'},
                          ]
            }
class MyContactTPLView(TemplateView):
    template_name = 'weblog/contact.html'
    def get_context_data(self, **kwargs):
        my_context_data = super().get_context_data(**kwargs)
        my_context_data['page_title'] = "TMPL Contact page"
        my_context_data['greet']= "TEMP BV Contact Info Page"
        my_context_data['prof_list']= [{'name': 'nasr', 'office': '3h14', 'contact': 'by MIO'},
                          {'name': 'dirk', 'office': '3F.26 ', 'contact': 'ask him'},
                          {'name': 'andrew', 'office': '3h26', 'contact': 'by MIO'},
                          ]

        return my_context_data

def list_all (req):
    data = {'page_title': "List page",
            'greet': "list Posts data",
            'list_posts' : get_posts_2017(),
            'lst_stat': get_auth_posts_totals(5)
            }
    template_name= 'weblog/post_list.html'
    return render(req, template_name, context=data )

class MyListPostsView(View):
    model = Post
    template_name = 'weblog/post_list.html'

    def get(self, req, *arg, **kwargs):
        res = Post.objects.all()
        data = {'page_title': "CBV List page",
               'greet': "CBV list Posts data",
               'list_posts' : res
               }
        return render (req, self.template_name, data)

class MyListPostsTempView(TemplateView):
    model = Post
    template_name = 'weblog/post_list.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "CBV TEMP List page"
        context ['greet'] = "CBV list Posts data"
        context['list_posts'] = Post.objects.all()
        return context

from django.views.generic import ListView, DetailView

class MyListPosts (ListView):
    model = Post
    ordering = '-pub_date'
    paginate_by = 4
    # template_name = 'weblog/post_list.html'

    # contxt={'owner_post_stast' : get_auth_posts_totals(3)}
    # extra_context = contxt
    # queryset = Post.objects.filter(request.user=)
    def get_context_data(self, *args, **kwargs):
        contxt = super().get_context_data(*args, **kwargs)
        contxt ['key'] ='**value'

        contxt ['owner_post_stast'] =get_auth_posts_totals(5)
        return contxt




    # queryset = 'lst_stat': get_auth_posts_totals(5)


# class MyListAuthors (ListView):
#     model = Author
#     ordering = '-age'
#     paginate_by = 5


#---- detail view
class MyPostDetailView (View):
    model = Post
    template_name = 'weblog/my_detail_post.html'

    def get(self, req, *args, **kwarhs):
        my_post = get_object_or_404(Post , id=kwarhs['id'])
        return render (req, self.template_name, {'p': my_post})


class MyPostDetail(DetailView):
    model=Post




def author_book_stat(top_n):
    return []
    # return Author.objects.annotate(nber_posts=Count('post')).order_by('-nber_posts')[:top_n]



#-------   Post CRUD
# from django.views.generic.edit import CreateView, UpdateView, DeleteView
# from django.contrib.auth.mixins import LoginRequiredMixin,UserPassesTestMixin
#
# class PostCreateClassView(LoginRequiredMixin, CreateView):
#     model = Post
#     fields = ('title', 'body')
#
#     def form_valid(self, form):
#         form.instance.user = self.request.user
#         return super().form_valid(form)
#
#     def get_context_data(self, **kwargs):
#         context= super().get_context_data(**kwargs)
#         context['button_value']="New Post"
#         context['legend'] = "Create  a  new post"
#         return context
#
#
#---   CRUD using functions
from .forms import PostCreateForm
from django.contrib.auth.decorators import login_required

# @login_required(login_url='member_login')
# def create_post_view(request):
#     # if not request.user.is_authenticated:
#     #     return redirect('member_login')
#
#     print ('test 1')
#     template_name = 'weblog/post_form.html'
#     if request.method=="POST":
#         form = PostCreateForm(request.POST)
#         print(f" debug here:{form.is_valid()}")
#         if form.is_valid():
#             ttile = form.cleaned_data['title']
#             body = form.cleaned_data['body']
#             this_user = Member.objects.get(user = request.user)
#             Post.objects.create(title= ttile, body= body, owner=this_user)
#
#             messages.success(request, f" created post {ttile[:10]}...")
#             return redirect('weblog_list_all')
#         else:
#             messages.error(request, f" something went wrong...")
#     else:
#         print('test 2')
#         form = PostCreateForm()
#     return render(request,
#                   template_name,
#                   {'form':form,
#                    'btn_val': "NEW POST",
#                    'for_toitl':"CREATING a new Post"})
#
#
# @login_required(login_url='member_login')
# def update_post_view(request, pk):
#     template_name='weblog/post_form.html'
#     post2_update = Post.objects.get(id=pk)
#     if request.user != post2_update.owner.user:
#         messages.success(request, f"  YOUR ARE NOT ALLWED...")
#         return redirect('weblog_list_all')
#
#     if request.method=="POST":
#         form = PostCreateForm(request.POST)
#         print(f" debug here:{form.is_valid()}")
#         if form.is_valid():
#             ttile = form.cleaned_data['title']
#             body = form.cleaned_data['body']
#
#             #updating process
#             post2_update.title= ttile
#             post2_update.body = body
#             post2_update.save()
#
#
#             messages.success(request, f" updated the post {ttile[:10]}...")
#             return redirect('weblog_list_all')
#         else:
#             messages.error(request, f" something went wrong...")
#     else:
#         form = PostCreateForm(instance=post2_update)
#     return render(request,
#                   template_name,
#                   {'form':form,
#                    'btn_val': "EDIT POST",
#                    'for_toitl':"UPDATING a new Post"})
#
# from django.contrib.auth.decorators import login_required
#
# @login_required
# def delete_post_view(request, pk):
#
#
#     template_name='weblog/post_confirm_delete.html'
#     post2_delete = Post.objects.get(id=pk)
#     if request.user != post2_delete.owner.user:
#         messages.success(request, f"  YOUR ARE NOT ALLWED...")
#         return redirect('weblog_list_all')
#     if request.method=="POST":
#             #deleting process
#             post2_delete.delete()
#             messages.success(request, f" deleted the post {post2_delete.title[:10]}...")
#             return redirect('weblog_list_all')
#
#     return render(request,
#                   template_name,
#                   {                   'btn_val': "EDIT POST",
#                    'for_toitl':"UPDATING a new Post"})

#---   CRUD using CBV

from django.views.generic import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy


class CreatePostClassView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Post
    fields = ('title', 'body')
    success_url = reverse_lazy('weblog_list_all')
    success_message = "You justr crate a post!"
    extra_context = {
        'for_toitl': "CREATE USING CBV",
        'btn_val': "CREAT PPPOST"
    }

    def form_valid(self, form):
         form.instance.owner =  Member.objects.get(user= self.request.user)
         return super().form_valid( form)

class UpdatePostClassView(LoginRequiredMixin, SuccessMessageMixin,
                          UserPassesTestMixin, UpdateView):
    model = Post
    fields = ('title', 'body')
    success_url = reverse_lazy('weblog_list_all')
    success_message = "You justr updated a post!"
    extra_context = {
        'for_toitl': "Update USING CBV",
        'btn_val': "Update PPPOST"
    }

    def test_func(self):
        current_post = self.get_object()
        return self.request.user == current_post.owner.user


class RemovePostClassView(LoginRequiredMixin, SuccessMessageMixin,
                          UserPassesTestMixin, DeleteView):
    model = Post
    success_url = reverse_lazy('weblog_list_all')
    success_message = "You justr deleted a post!"

    def test_func(self):
        current_post = self.get_object()
        return self.request.user == current_post.owner.user

    # ---------------  Rest api ----
from rest_framework import status
from rest_framework.response import Response

from rest_framework.decorators import api_view

from .models import Post
from .serailizer import PostClassSerialize

@api_view(['GET'])
def api_map(req):
        my_api_urls = {
            'List': 'api/post-list',  # "<a href ='{% url
            'Detail': 'api/<int:pk>/post-detail/',
            'Create': 'api/post-new/',
            'Update': 'api/post-edit/<int:pk>',
            'Delete': 'api/<int:pk>/post-delete',
        }
        return Response(my_api_urls)

@api_view(['GET'])
# @login_required()
def api_get_all_posts(req):
        print(f"req.user.is_authenticated:{req.user.is_authenticated}")

        posts = Post.objects.all()
        obj_serializer = PostClassSerialize(posts, many=True)
        return Response(obj_serializer.data)

@api_view(['POST'])
@login_required
def api_create_post(req):
        if not req.user.is_authenticated:
            return redirect('member_login')
        obj_serializer = PostClassSerialize(data=req.data)
        if obj_serializer.is_valid():
            obj_serializer.save()
            return Response(obj_serializer.data, status=status.HTTP_201_CREATED)
        return Response(obj_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def get_post(pk):
        try:
            the_post = Post.objects.get(id=pk)
            return the_post
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['GET'])
def api_post_detail(req, pk):
        the_post = get_post(pk)
        obj_serializer = PostClassSerialize(instance=the_post)  # , many=False)
        return Response(obj_serializer.data)

@api_view(['POST'])
def api_edit_post(req, pk):
        the_post = get_post(pk)
        obj_serializer = PostClassSerialize(instance=the_post, data=req.data)
        if obj_serializer.is_valid():
            obj_serializer.save()
            return Response(obj_serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(obj_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def api_delete_post(req, pk):
        the_post = get_post(pk)
        the_post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

