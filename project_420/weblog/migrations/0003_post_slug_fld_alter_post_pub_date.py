# Generated by Django 4.0.3 on 2022-04-20 16:13

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('weblog', '0002_alter_post_pub_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='slug_fld',
            field=models.SlugField(null=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 4, 20, 12, 13, 58, 790751)),
        ),
    ]
