# Generated by Django 4.0.3 on 2022-04-20 16:32

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('weblog', '0003_post_slug_fld_alter_post_pub_date'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='slug_fld',
            new_name='slug',
        ),
        migrations.AlterField(
            model_name='post',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2022, 4, 20, 12, 32, 49, 838043)),
        ),
    ]
