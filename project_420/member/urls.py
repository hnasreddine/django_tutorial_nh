from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path
from . import views
urlpatterns = [
    path ('', views.home, name='member_home' ),
    path ('home', views.home, name='member_home' ),
    path ('login', LoginView.as_view( template_name="member/login.html" ),
            name='member_login' ),
    path ('logout', LogoutView.as_view( template_name="member/logout.html" ),
            name='member_logout' ),
    # path ('register', views.register__,  name='member_register' ),
    path('register', views.RegisterUserClassView.as_view(), name='member_register'),
    path('edit', views.EditUserClassView.as_view() , name='member_edit' ),

]