from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    affiliation = models.CharField(max_length=120, help_text='enter user affiliation')
    user_pic = models.ImageField(upload_to='user_pics',
                                 default='default_image.png')


    def __str__(self):
        return self.user.username

