from django.contrib import messages
# from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect


# Create your views here.
from django.urls import  reverse_lazy
from django.views.generic import CreateView, UpdateView

from .forms import UserRegistrationForm, MemberForm, UserEditForm
from .models import Member


def home(req):
    template_name='member/home.html'
    print (f"req.user.username {req.user.username}")
    amember = Member.objects.get(user__username = req.user.username)
    context = {'amember': amember}

    return render (req, template_name, context)

def register__ (request):
    template_name = 'member/register.html'
    if (request.method=="POST"):
        print (f"type post {type(request.POST)}")
        reg_form = UserRegistrationForm(request.POST)
        member_form = MemberForm(request.POST, request.FILES)

        if reg_form.is_valid():
            username=reg_form.cleaned_data.get('username')
            reg_form.save()  # save user data (username, pass, email., ...
            if not member_form.is_valid():
                raise ValueError(f" error with pic uplaod ...")
            else:
                affiliation = member_form.cleaned_data.get('affiliation')
                user_pic =  member_form.cleaned_data.get('user_pic')
                the_user = User.objects.get(username =username)
                Member.objects.create(affiliation = affiliation, user= the_user,
                                      user_pic= user_pic )
                messages.success(request, f"Accout successfuly created fro {username}")
                return redirect ('member_login')
        else:
            messages.error(request, f"some error while creating account")
    else:
        reg_form = UserRegistrationForm()
        member_form = MemberForm()

    return render (request, template_name, {'reg_form':reg_form, 'member_form': member_form})

#----------
class RegisterUserClassView(CreateView):
    form_class = UserRegistrationForm
    success_url = reverse_lazy('member_login')
    template_name = 'member/register.html'

    def get(self, request, *args, **kwargs):
        reg_form = UserRegistrationForm()
        mmber_form = MemberForm()
        return render(request, self.template_name,
                        {'reg_form': reg_form, 'member_form':mmber_form})

    def post(self, request, *args, **kwargs):
        reg_form = UserRegistrationForm(request.POST)
        mmber_form = MemberForm(request.POST, request.FILES)
        if reg_form.is_valid():
            username = reg_form.cleaned_data['username']
            reg_form.save()  # save user data (username, pass, email., ...
            if mmber_form.is_valid():
                affiliation = mmber_form.cleaned_data.get('affiliation')
                user_pic = mmber_form.cleaned_data.get('user_pic')
                this_user = User.objects.get(username = username)
                Member.objects.create(affiliation=affiliation, user_pic= user_pic ,
                              user=this_user)
                messages.success(request, f"Accout successfuly created fro {username}")
                return redirect('member_login')
            else:
                messages.error(request, f"Error occured (may be file uload problem)for  {username}")
        return render (request, self.template_name, {'reg_form':reg_form, 'member_form': mmber_form})



class EditUserClassView(UpdateView):
    form_class = UserEditForm
    success_url = reverse_lazy('member_home')
    template_name = 'member/member_edit.html'

    def get(self, request, *args, **kwargs):
        user_data = {'email':request.user.email,
                     'last_name': request.user.last_name,
                     'first_name': request.user.first_name}

        edit_form = UserEditForm(user_data)
        this_member = Member.objects.get(user =request.user )
        member_data = {'affiliation': this_member.affiliation,
                      'user_pic': this_member.user_pic.url}
        mmber_form = MemberForm(member_data)
        return render(request, self.template_name,
                        {'edit_form': edit_form, 'member_form':mmber_form})

    def post(self, request, *args, **kwargs):
        edit_form = UserEditForm(request.POST)
        mmber_form = MemberForm(request.POST, request.FILES)
        if edit_form.is_valid():
            # username = edit_form.cleaned_data['username']
            emil = edit_form.cleaned_data['email']
            request.user.email = emil
            request.user.last_name = edit_form.cleaned_data.get ('last_name')
            request.user.last_name = edit_form.cleaned_data.get ('first_name')
            request.user.save()
            if mmber_form.is_valid():
                affiliation = mmber_form.cleaned_data.get('affiliation')
                user_pic = mmber_form.cleaned_data.get('user_pic')
                this_member = Member.objects.get(user =request.user )
                this_member.affiliation = affiliation
                this_member.user_pic = user_pic
                this_member.save()
                messages.success(request, f"Profile  successfuly updated  fro {request.user.username}")
                return redirect('member_login')
            else:
                messages.error(request, f"Error occured (may be file uload problem)for  {request.user.username}")
        else:
            messages.error(request, f"Error occured for email/lname,fname)for  {request.user.username}")
        return render (request, self.template_name, {'edit_form':edit_form, 'member_form': mmber_form})



